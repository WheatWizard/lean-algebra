namespace zoo
universe u

class left_unital {α : Type u} (bin : α → α → α) :=
  ( unit : α )
  ( left_id : ∀ a : α, bin unit a = a )

open left_unital

class right_unital {α : Type u} (bin : α → α → α) :=
  ( right_unit : α )
  ( right_id' : ∀ a : α, bin a right_unit = a )

open right_unital

class unital {α : Type u} (bin : α → α → α) extends left_unital bin, right_unital bin

open unital

instance left_right_unital {α : Type u} (bin : α → α → α) [left_unital bin] [right_unital bin] : unital bin :=
  {}

lemma units_eq {α : Type u} (bin : α → α → α) [unital bin] [r : right_unital bin] [l : left_unital bin] : unit bin = right_unit bin := by
  { conv
    { to_lhs
    , rw <- @right_unital.right_id' α bin r (unit bin)
    }
  , conv
    { to_rhs
    , rw <- @left_unital.left_id α bin l (right_unit bin)
    }
  }

theorem right_id {α : Type u} (bin : α → α → α) [unital bin] : ∀ a : α, bin a (unit bin) = a := λ a, by
  { rw units_eq
  , rw @right_unital.right_id' α bin
  }

class semigroup {α : Type u} (bin : α → α → α) :=
  ( assoc : ∀ a b c : α, bin a (bin b c) = bin (bin a b) c )

open semigroup

class quasigroup {α : Type u} (bin : α → α → α) :=
  ( left_div : α → α → α )
  ( right_div : α → α → α )
  ( left_cancel : ∀ a b : α, bin a (left_div a b) = b )
  ( right_cancel : ∀ a b : α, bin (right_div b a) a = b )

open quasigroup

class monoid {α : Type u} (bin : α → α → α) extends semigroup bin, unital bin

instance monoid_def {α : Type u} {bin : α → α → α} [semigroup bin] [unital bin] : monoid bin := {}

class loop {α : Type u} (bin : α → α → α) extends quasigroup bin, unital bin

instance loop_def {α : Type u} {bin : α → α → α} [quasigroup bin] [unital bin] : loop bin := {}

class group {α : Type u} (bin : α → α → α) extends quasigroup bin, semigroup bin, unital bin :=
  ( neg : α → α )
  ( left_inv : ∀ a : α, bin (neg a) a = unit )
  ( right_inv : ∀ a : α, bin a (neg a) = unit )

lemma left_right_cancellation {α : Type u} {bin : α → α → α} [q : quasigroup bin] [s : semigroup bin] [u : unital bin] : ∀ a : α, bin a (right_div bin (unit bin) a) = unit bin := λ a, by
  {  rw <- (@right_id α bin u) (bin a (right_div bin (unit bin) a))
  , conv
    { to_lhs
    , congr
    , skip
    , rw <- (@left_cancel α bin q a (unit bin))
    }
  , rw <- (@assoc α bin s)
  , conv
    { to_lhs
    , congr
    , skip
    , rw (@assoc α bin)
    , rw (@right_cancel α bin)
    , rw (@left_id α bin)
    }
  , rw (@left_cancel α bin q)
  }

instance group_def {α : Type u} {bin : α → α → α} [q : quasigroup bin] [s : semigroup bin] [u : unital bin] : group bin :=
  ⟨ @right_div α bin q (unit bin)
  , λ a, right_cancel a (unit bin)
  , left_right_cancellation
  ⟩

class left_semigroup_act {α β : Type u} (bin : α → α → α) (φ : α → β → β) extends semigroup bin :=
  ( left_compatibility : ∀ a b : α, ∀ c : β, φ a (φ b c) = φ (bin a b) c )

class right_semigroup_act {α β : Type u} (bin : α → α → α) (φ : β → α → β) extends semigroup bin :=
  ( right_compatibility : ∀ a : β, ∀ b c : α, φ a (bin b c) = φ (φ a b) c )

instance semigroup_left_self_act {α : Type u} {bin : α → α → α} [s : semigroup bin] : left_semigroup_act bin bin :=
  ⟨ assoc
  ⟩

instance semigroup_right_self_act {α : Type u} {bin : α → α → α} [s : semigroup bin] : right_semigroup_act bin bin :=
  ⟨ assoc
  ⟩

class left_monoid_act {α β : Type u} (bin : α → α → α) (φ : α → β → β) extends unital bin, left_semigroup_act bin φ :=
  ( weak_left_id : ∀ a : β, φ unit a = a)

instance monoid_left_self_act {α : Type u} {bin : α → α → α} [s : left_semigroup_act bin bin] [u : unital bin] : left_monoid_act bin bin :=
  ⟨ left_id
  ⟩

class right_monoid_act {α β : Type u} (bin : α → α → α) (φ : β → α → β) extends unital bin, right_semigroup_act bin φ :=
  ( weak_right_id : ∀ a : β, φ a unit = a)

instance monoid_right_self_act {α : Type u} {bin : α → α → α} [s : right_semigroup_act bin bin] [u : unital bin] : right_monoid_act bin bin :=
  ⟨ right_id bin
  ⟩

end zoo
